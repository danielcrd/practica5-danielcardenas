package clases;

import java.time.LocalDate;

public class Trabajador {

	/*
	 * Creamos los atributos, modificador en tipo privado eso quiere decir que no
	 * son accesibles desde ninguna otra clase
	 */

	private String nombre;
	private String usuario;
	private String password;
	private LocalDate fechaNacimiento;
	private LocalDate inicioContrato;
	private String sala;
	private Float salario;

	// Constructor, para poder crear un objeto o instancia de la clase de trabajador

	public Trabajador(String nombre, String usuario, String password) {
		this.nombre = nombre;
		this.usuario = usuario;
		this.password = password;
	}

	// Metodos getters and setters, imprescindibles para poder visualizar o cambiar
	// los valores de los atributos

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public LocalDate getInicioContrato() {
		return inicioContrato;
	}

	public void setInicioContrato(LocalDate inicioContrato) {
		this.inicioContrato = inicioContrato;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public Float getSalario() {
		return salario;
	}

	public void setSalario(Float salario) {
		this.salario = salario;
	}

	// toString, Devuelve la representacion en String del objeto

	@Override
	public String toString() {
		return "Trabajador [nombre=" + nombre + ", usuario=" + usuario + ", password=" + password + ", fechaNacimiento="
				+ fechaNacimiento + ", inicioContrato=" + inicioContrato + ", sala=" + sala + ", salario=" + salario
				+ "]";
	}

}
