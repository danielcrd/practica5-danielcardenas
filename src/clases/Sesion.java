package clases;

import java.time.LocalDate;

public class Sesion {

	/*
	 * Creamos los atributos, modificador en tipo privado eso quiere decir que no
	 * son accesibles desde ninguna otra clase
	 */

	private LocalDate fechaCompra;
	private String nombre;
	private String horaInicio;
	private String horaFinal;
	private float precio;
	private String estado;
	private String cliente;
	private int n_materiales;
	private Trabajador encargadoSesion;

	// Constructor, para poder crear un objeto o instancia de la clase de trabajador

	public Sesion(String nombre, String estado, String cliente) {
		this.nombre = nombre;
		this.estado = estado;
		this.cliente = cliente;
	}
	// Metodos getters and setters, imprescindibles para poder visualizar o cambiar
	// los valores de los atributos

	public LocalDate getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public int getN_materiales() {
		return n_materiales;
	}

	public void setN_materiales(int n_materiales) {
		this.n_materiales = n_materiales;
	}

	public Trabajador getEncargadoSesion() {
		return encargadoSesion;
	}

	public void setEncargadoSesion(Trabajador encargadoSesion) {
		this.encargadoSesion = encargadoSesion;
	}

	// toString, Devuelve la representacion en String del objeto

	@Override
	public String toString() {
		return "Sesion [fechaCompra=" + fechaCompra + ", nombre=" + nombre + ", horaInicio=" + horaInicio
				+ ", horaFinal=" + horaFinal + ", precio=" + precio + ", estado=" + estado + ", cliente=" + cliente
				+ ", n_materiales=" + n_materiales + "]";
	}

}