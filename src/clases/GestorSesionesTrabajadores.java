package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author Daniel Cárdenas
 *
 */

public class GestorSesionesTrabajadores {

	/**
	 * Creamos los atributos, modificador en tipo privado eso quiere decir que no
	 * son accesibles desde ninguna otra clase
	 */

	private ArrayList<Trabajador> trabajadores;
	private ArrayList<Sesion> sesiones;

	/**
	 * Creamos el constructor, donde crearemos el ArrayList de trabajadores y
	 * sesiones
	 */

	public GestorSesionesTrabajadores() {
		trabajadores = new ArrayList<Trabajador>();
		sesiones = new ArrayList<Sesion>();
	}

	/**
	 * Metodo altaTrabajador, se utiliza para poder crear un trabajador y añadirlo
	 * en el arrayList
	 * 
	 * @param nombre          -- nombre del trabajador
	 * @param usuario         -- usuario del trabajador
	 * @param password        -- contraseña del trabajador
	 * @param fechaNacimiento -- fecha de nacimiento del trabajador
	 * @param sala            -- sala en la cual trabajara el trabajador
	 * @param salario         -- salario del trabajador
	 * @return void
	 */

	public void altaTrabajador(String nombre, String usuario, String password, String fechaNacimiento, String sala,
			float salario) {
		if (!comprobarExisteTrabajador(usuario)) {
			Trabajador trabajador = new Trabajador(nombre, usuario, password);
			trabajador.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
			trabajador.setSala(sala);
			trabajador.setInicioContrato(LocalDate.now());
			trabajador.setSalario(salario);
			trabajadores.add(trabajador);
		} else {
			System.out.println("El trabajador que esta intentando dar de alta ya existe");
		}
	}

	/**
	 * Metodo listar todos los trabajadores, he utilizado la forma para recorrer
	 * mediante el for each
	 * 
	 * @return void
	 */

	public void listarTrabajador() {
		for (Trabajador trabajador : trabajadores) {
			if (trabajador != null) {
				System.out.println(trabajador);
			}
		}
	}

	/**
	 * Metodo para comprobar si existe el trabajador, si el valor es nulo o el
	 * usuario registrado ya esta dado de alta no podra realizarlo.
	 * 
	 * @param usuario -- usuario del trabajador para comprobar si existe
	 * @return boolean
	 */

	public boolean comprobarExisteTrabajador(String usuario) {
		for (Trabajador trabajador : trabajadores) {
			if (trabajador != null && trabajador.getUsuario().equalsIgnoreCase(usuario)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para poder eliminar un trabajador, pedimos un usuario, llamamos al
	 * metodo y comprobaremos si existe el usuario o no, si es asi, lo eliminara, si
	 * no es asi, devolvera el mensaje 'trabajador no encontrado'
	 * 
	 * @param usuario -- usuario del trabajador a eliminar
	 * @return void
	 */

	public void eliminarTrabajador(String usuario) {
		Iterator<Trabajador> iterador = trabajadores.iterator();
		int contador = 0;
		while (iterador.hasNext()) {
			Trabajador trabajador = (Trabajador) iterador.next();
			if (trabajador.getUsuario().equalsIgnoreCase(usuario)) {
				iterador.remove();
				contador++;
				break;
			}
		}
		if (contador == 0) {
			System.out.println("Trabajador no encontrado");
		}

	}

	/**
	 * metodo en el cual funcionara para buscar un trabajador con el usuario, si no
	 * encuentra el usuario nos dara un null
	 * 
	 * @param usuario -- usuario del trabajador a buscar
	 * @return Trabajador
	 */

	public Trabajador buscarTrabajador(String usuario) {
		for (Trabajador trabajador : trabajadores) {
			if (trabajador != null && trabajador.getUsuario().equalsIgnoreCase(usuario)) {
				return trabajador;
			}
		}
		return null;
	}

	/**
	 * Con este metodo conseguiremos el trabajador con menos salario y mostraremos
	 * tanto su nombre como su salario
	 * 
	 * @return void
	 */

	public void trabajadorConMenorSalario() {
		String nombreTrabajador = trabajadores.get(0).getNombre();
		float salarioTrabajador = trabajadores.get(0).getSalario();
		for (int i = 0; i < trabajadores.size(); i++) {
			if (salarioTrabajador > trabajadores.get(i).getSalario()) {
				salarioTrabajador = trabajadores.get(i).getSalario();
				nombreTrabajador = trabajadores.get(i).getNombre();
			}
		}

		if (trabajadores.size() == 0) {
			System.out.println("Aun no se han registrado ningún trabajador");
		} else {
			System.out.println("El trabajador: " + nombreTrabajador + " tiene el menor salario con un total de: "
					+ salarioTrabajador);
		}
	}

	/**
	 * Con este metodo sacaremos el nombre de las salas de cada uno de nuestros
	 * trabajadores
	 * 
	 * @return void
	 */

	public void salaDeCadaTrabajador() {
		for (Trabajador trabajador : trabajadores) {
			System.out.println(
					"El trabajador: " + trabajador.getNombre() + " trabaja en la sala: " + trabajador.getSala());
		}
	}

	/**
	 * Metodo en el cual recibira el usuario del trabajador y te mostrara cuantas
	 * sesiones estan asignadas a su usuario
	 * 
	 * @param usuario -- usuario del trabajador para comprobar sus sesiones
	 * @return void
	 */

	public void numeroSesionesPorTrabajador(String usuario) {
		int contador = 0;
		for (Sesion sesion : sesiones) {
			if (sesion.getEncargadoSesion() != null && sesion.getEncargadoSesion().getUsuario().equals(usuario)) {
				contador++;
			}
		}
		if (!comprobarExisteTrabajador(usuario)) {
			System.out.println("No existe el trabajador");
		} else if (contador == 0) {
			System.out.println("Este trabajador no tiene sesiones aun registradas a su usuario");
		} else if (contador == 1) {
			System.out.println("El trabajador " + usuario + " tiene " + contador + " sesion asignada");
		} else {
			System.out.println("El trabajador " + usuario + " tiene " + contador + " sesiones asignadas");
		}
	}

	// ***************************** METODOS DE SESIONES
	// *****************************************************************

	/**
	 * Metodo listar todas las sesiones, he utilizado la forma para recorrer
	 * mediante el uso del for each
	 * 
	 * @return void
	 */
	public void listarSesiones() {
		for (Sesion sesion : sesiones) {
			if (sesion != null) {
				System.out.println(sesion);
			}
		}
	}

	/**
	 * Metodo para crear una sesion, comprobando a la vez si existe mediante un if,
	 * si existe nos saldra un mensaje diciendo que ya existe dicha sesion, si no es
	 * asi, nos creara una nueva sesión
	 * 
	 * @param nombre       -- nombre de la sesión
	 * @param horaInicio   -- hora de inicio de la sesión
	 * @param horaFinal    -- hora de finalización de la sesión
	 * @param precio       -- precio de la sesión
	 * @param estado       -- estado de la sesión
	 * @param clinete      -- nombre del cliente de la sesión
	 * @param n_materiales -- número de materiales usadas en la sesión
	 * @return void
	 */

	public void altaSesion(String nombre, String horaInicio, String horaFinal, float precio, String estado,
			String cliente, int n_materiales) {
		if (existeSesion(nombre)) {
			System.out.println("Esta sesion ya se registro!");
		} else {
			Sesion sesion = new Sesion(nombre, estado, cliente);
			sesion.setHoraInicio(horaInicio);
			sesion.setHoraFinal(horaFinal);
			sesion.setEstado(estado);
			sesion.setPrecio(precio);
			sesion.setN_materiales(n_materiales);
			sesion.setFechaCompra(LocalDate.now());
			sesiones.add(sesion);
		}
	}

	/**
	 * para evitar mucho codigo en un mismo metodo, realizamos un metodo solo
	 * especificamente para comprobar si existe una sesión, si el nombre de la
	 * sesion es igual a alguna de las sesiones ya dadas de alta, devolvera un true,
	 * si no es asi dara un booleano tipo falso
	 * 
	 * @param nombre -- nombre de la sesión
	 * @return boolean
	 */

	public boolean existeSesion(String nombre) {
		for (Sesion sesion : sesiones) {
			if (sesion != null & sesion.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Con este metodo conseguiremos poder eliminar una sesión gracias al nombre que
	 * introduzcamos, ya que, dicho nombre sera comprobado uno por uno por cada
	 * sesion dada de alta, si dicho nombre no es igual al nombre de alguna sesión,
	 * el propio metodo devolvera un mensaje diciendo que dicha sesión no ha sido
	 * encontrada
	 * 
	 * @param nombre -- nombre de la sesión
	 * @return void
	 */

	public void eliminarSesion(String nombre) {
		Iterator<Sesion> iterador = sesiones.iterator();
		int contador = 0;
		while (iterador.hasNext()) {
			Sesion sesion = (Sesion) iterador.next();
			if (sesion.getNombre().equalsIgnoreCase(nombre)) {
				iterador.remove();
				contador++;
				break;
			}
		}
		if (contador == 0) {
			System.out.println("Sesión no encontrada");
		}
	}

	/**
	 * Con este metodo conseguiremos encontrar una Sesion, y mostrarla al usuario,
	 * si por un casual dicho nombre no existe(las sesiones se identifican por
	 * nombres), el metodo devolvera un null, que traducido en la clase programa,
	 * sera un mensaje aclarando que no existe dicha sesión
	 * 
	 * @param nombre -- nombre de la sesión
	 * @return Sesion
	 */

	public Sesion buscarSesion(String nombre) {
		for (Sesion sesion : sesiones) {
			if (sesion != null && sesion.getNombre().equalsIgnoreCase(nombre)) {
				return sesion;
			}
		}
		return null;
	}

	/**
	 * Con este metodo conseguiremos mostrar un único atributo de la sesión
	 * especificada con el nombre, recorreremos todas las sesiones y comprobaremos
	 * si existe dicha sesión, si no es así nos dara un mensaje de no encontrado,
	 * luego mediante el switch, dependiendo del atributo nos dara un mensaje o
	 * otro.
	 * 
	 * @param nombre -- nombre de la sesión
	 * @param valor  -- atributo que desea mostrar
	 * @return void
	 */

	public void ListarSesionPorAtributo(String nombre, String valor) {
		int contador = 0;
		for (Sesion sesion : sesiones) {
			if (existeSesion(nombre) && sesion.getNombre().equalsIgnoreCase(nombre)) {
				contador++;
				switch (valor) {
				case "fechacompra":
					System.out.println("La fecha de compra es: " + sesion.getFechaCompra());
					break;
				case "nombre":
					System.out.println("El nombre de la sesión es: " + sesion.getNombre());
					break;
				case "horainicio":
					System.out.println("La hora de inicio de la sesión es: " + sesion.getHoraInicio());
					break;
				case "horafinal":
					System.out.println("La hora final de la sesión es: " + sesion.getHoraFinal());
					break;
				case "precio":
					System.out.println("El precio de la sesión es: " + sesion.getPrecio());
					break;
				case "estado":
					System.out.println("El estado de la sesión es: " + sesion.getEstado());
					break;
				case "n_materiales":
					System.out.println("Número de materiales de la sesión: " + sesion.getN_materiales());
					break;
				case "cliente":
					System.out.println("El cliente de la sesión: " + sesion.getCliente());
					break;
				default:
					System.out.println("El atributo puesto no existe");
					break;
				}
			}
		}
		if (contador == 0) {
			System.out.println("La sesión no existe");
		}
	}

	/**
	 * Con este metodo sacaremos todas las sesiones que tiene el trabajador,
	 * especificando su usuario, comprobaremos de cada una de las sesiones si el
	 * usuario que se encarga de dicha sesión, es igual al usuario introducido, si
	 * es asi mostraremos la sesión
	 * 
	 * @param usuario -- usuario del trabajador el cual queremos mostrar sus
	 *                sesiones
	 * @return void
	 */

	public void listarSesionesPorUsuarioTrabajador(String usuario) {
		System.out.println("El trabajador con el usuario " + usuario + " tiene estas sesiones asignadas: ");
		int contador = 0;
		for (Sesion sesion : sesiones) {
			if (sesion.getEncargadoSesion() != null
					&& sesion.getEncargadoSesion().getUsuario().equalsIgnoreCase(usuario)) {
				System.out.println(sesion);
				contador++;
			}
		}
		if (contador == 0) {
			System.out.println("Este trabajador no tiene sesiones");
		}
	}

	/**
	 * Metodo en el cual asignaremos un trabajador a una sesión, tendremos varias
	 * comprobaciones y nos sacaran mensajes como: no encuentra la sesión, no
	 * encuentra el trabajador o ambas cosas, se comprobara con el usuario del
	 * trabajador y el nombre del cliente
	 * 
	 * @param usuario -- usuario del trabajador que queremos asignar la sesión
	 * @param nombre  -- nombre de la sesión que queremos asignar
	 * @return -- void
	 */

	public void asignarTrabajadorAUnaSesion(String usuario, String nombre) {
		if (existeSesion(nombre) && comprobarExisteTrabajador(usuario)) {
			Sesion sesion = buscarSesion(nombre);
			Trabajador trabajador = buscarTrabajador(usuario);
			sesion.setEncargadoSesion(trabajador);
		} else if (!existeSesion(nombre)) {
			System.out.println("No existe la sesión");
		} else if (!comprobarExisteTrabajador(usuario)) {
			System.out.println("No existe el trabajador");
		} else {
			System.out.println("No existe ni la sesión ni el trabajador");
		}
	}

	/**
	 * Con este metodo sacaremos la sesión mas cara de todas, en la cual nos
	 * mostrara tanto su nombre como su precio, para ello guardaremos el nombre de
	 * la sesión y su precio en las dos variables creadas a continuación
	 * 
	 * @return void
	 */

	public void sesionMasCara() {
		float precioMasCaro = 0;
		String nombreSesion = "";
		for (int i = 0; i < sesiones.size(); i++) {
			if (precioMasCaro < sesiones.get(i).getPrecio()) {
				precioMasCaro = sesiones.get(i).getPrecio();
				nombreSesion = sesiones.get(i).getNombre();
			}
		}
		if (sesiones.size() == 0) {
			System.out.println("Aun no se han creado ninguna sesión");
		} else {
			System.out.println("La sesión más cara es: " + nombreSesion + " con un valor de: " + precioMasCaro);
		}
	}

}
