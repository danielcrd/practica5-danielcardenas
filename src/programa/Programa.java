package programa;

import java.util.Scanner;

import clases.GestorSesionesTrabajadores;

public class Programa {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// Bienvenido a mi programa!!!!
		System.out.println("*****Bienvenido a mi programa!!!!*****");

		// Puntos que hay que realizar para el trabajo

		// 1- Crear instancia de GestorClases.
		GestorSesionesTrabajadores gestorFisio = new GestorSesionesTrabajadores();
		System.out.println("1- Crear instancia de GestorClases.");
		System.out.println("*****************************************************************");

		// 2- Dar de alta 3 elementos de la clase Trabajador y listar.
		System.out.println("2- Dar de alta 3 elementos de la clase Trabajador y listar.");
		gestorFisio.altaTrabajador("Daniel", "dani", "123456", "1998-09-06", "Sala 2", 800.67f);
		gestorFisio.altaTrabajador("Marcos", "marcos", "1234", "2000-12-21", "Sala 3", 900.67f);
		gestorFisio.altaTrabajador("Pepe", "pepe", "12345", "1990-01-01", "Sala 4", 1800.67f);
		gestorFisio.listarTrabajador();

		System.out.println("*****************************************************************");
		// 3- Buscar un elemento de Trabajador.
		System.out.println("3- Buscar un elemento de Trabajador.");
		System.out.println(gestorFisio.buscarTrabajador("dani"));
		System.out.println("*****************************************************************");
		// 4- Eliminar un elemento de Trabajador y listar.
		System.out.println("4- Eliminar un elemento de Trabajador y listar.");
		gestorFisio.eliminarTrabajador("dani");
		gestorFisio.listarTrabajador();
		System.out.println("*****************************************************************");
		// 5- Dar de alta 3 elementos de Sesiones y listar.
		System.out.println("5- Dar de alta 3 elementos de Sesiones y listar.");
		gestorFisio.altaSesion("Sesion masajes", "09:00", "09:45", 67.56f, "Activo", "Sandro mars", 5);
		gestorFisio.altaSesion("Sesion masajes 2", "12:00", "12:45", 77.56f, "Error", "Marcos sanz", 4);
		gestorFisio.altaSesion("Sesion masajes 3", "11:15", "12:00", 80.56f, "Desconocido", "Miguel sanr", 3);
		gestorFisio.listarSesiones();
		System.out.println("*****************************************************************");
		// 6- Buscar un elemento de Sesion.
		System.out.println("6- Buscar un elemento de Sesion.");
		System.out.println(gestorFisio.buscarSesion("Sesion masajes"));
		System.out.println("*****************************************************************");
		// 7- Eliminar un elemento de Sesion y listar.
		System.out.println("7- Eliminar un elemento de Sesion y listar.");
		gestorFisio.eliminarSesion("Sesion masajes");
		gestorFisio.listarSesiones();
		System.out.println("*****************************************************************");
		// 8- Lista elementos sesiones por alg�n atributo.
		System.out.println("8- Lista elementos sesiones por alg�n atributo.");
		gestorFisio.ListarSesionPorAtributo("Sesion masajes 2", "estado");
		System.out.println("*****************************************************************");
		// 9- Asignar un elemento que use Trabajador y Sesion y listar.
		System.out.println("9- Asignar un elemento que use Trabajador y Sesion y listar.");
		gestorFisio.asignarTrabajadorAUnaSesion("marcos", "Sesion masajes 2");
		System.out.println("*****************************************************************");
		// 10- Listar elementos Sesiones con un elemento de Trabajador.
		System.out.println("10- Listar elementos Sesiones con un elemento de Trabajador.");
		gestorFisio.listarSesionesPorUsuarioTrabajador("marcos");
		System.out.println("*****");

		/***** MENU *****/

		// Declaracion de variables para los menus
		int opcion;
		int opcionDos;
		int opcionTres;

		// Declaramos el string usuario y nombre, para poder utilizarlos en algunas
		// opciones, y asi evitar declararlos mas veces.
		String usuario;
		String nombre;

		do {
			System.out.println("***** MEN� *****");
			System.out.println("***** Con cual de las siguientes clases quieres interactuar *****");
			System.out.println("***** 1.- Trabajadores *****");
			System.out.println("***** 2.- Sesiones *****");
			System.out.println("***** 3.- Salir *****");
			opcion = input.nextInt();

			// ***** SWITCH para la opci�n a elegir *****
			System.out.println("*****");
			;
			switch (opcion) {
			case 1:

				do {

					System.out.println("***** Has elegido Trabajadores *****");
					System.out.println("1.- Dar de alta un trabajador");
					System.out.println("2.- Eliminar un trabajador");
					System.out.println("3.- Buscar un trabajador");
					System.out.println("4.- Listar todos los trabajadores");
					System.out.println("5.- Trabajador con menor salario");
					System.out.println("6.- En que sala trabajan cada uno de los trabajadores");
					System.out.println("7.- Cuantas sesiones tiene un trabajador");
					System.out.println("8.- Salir");
					System.out.println("*****");
					;

					opcionDos = input.nextInt();
					input.nextLine();
					switch (opcionDos) {
					case 1:
						System.out.println("1.- Dar de alta un trabajador");
						altaTrabajador(gestorFisio);
						break;
					case 2:
						System.out.println("2.- Eliminar un trabajador");
						System.out.println("Por favor introdzuca el usuario del trabajador: ");
						usuario = input.nextLine().toLowerCase();
						gestorFisio.eliminarTrabajador(usuario);
						break;
					case 3:
						System.out.println("3.- Buscar un trabajador");
						System.out.println("Por favor introdzuca el usuario del trabajador: ");
						usuario = input.nextLine().toLowerCase();
						if (gestorFisio.buscarTrabajador(usuario) == null) {
							System.out.println("El trabajador no existe");
						} else {
							System.out.println(gestorFisio.buscarTrabajador(usuario));
						}
						break;
					case 4:
						System.out.println("4.- Listar todos los trabajadores");
						gestorFisio.listarTrabajador();
						break;
					case 5:
						System.out.println("5.- Trabajador con menor salario");
						gestorFisio.trabajadorConMenorSalario();
						break;
					case 6:
						System.out.println("6.- En que sala trabajan cada uno de los trabajadores");
						gestorFisio.salaDeCadaTrabajador();
						break;
					case 7:
						System.out.println("7.- Cuantas sesiones tiene un trabajador");
						System.out.println("Por favor introdzuca el usuario del trabajador: ");
						usuario = input.nextLine().toLowerCase();
						gestorFisio.numeroSesionesPorTrabajador(usuario);
						break;
					case 8:
						System.out.println("8.- Salir");
						break;
					default:
						System.out.println("Opci�n no valida");
						break;
					}
				} while (opcionDos != 8);

				System.out.println("*****");
				;
				break;
			case 2:

				do {

					System.out.println("***** Has elegido Sesiones *****");
					System.out.println("1.- Dar de alta una sesi�n");
					System.out.println("2.- Eliminar una sesi�n");
					System.out.println("3.- Buscar una sesi�n");
					System.out.println("4.- Listar todas las sesiones");
					System.out.println("5.- Listar las sesiones de un trabajador");
					System.out.println("6.- Asignar una sesi�n a un trabajador");
					System.out.println("7.- Listar un atributo de una sesi�n");
					System.out.println("8.- Sesi�n mas cara");
					System.out.println("9.- Salir");
					System.out.println("*****");
					;

					opcionTres = input.nextInt();
					input.nextLine();
					switch (opcionTres) {
					case 1:
						System.out.println("1.- Dar de alta una sesi�n");
						altaSesion(gestorFisio);
						break;
					case 2:
						System.out.println("2.- Eliminar una sesi�n");
						System.out.println("Por favor introduzca el nombre de la sesi�n");
						nombre = input.nextLine().toLowerCase();
						gestorFisio.eliminarSesion(nombre);
						break;
					case 3:
						System.out.println("3.- Buscar una sesi�n");
						System.out.println("Por favor introduzca el nombre de la sesi�n");
						nombre = input.nextLine().toLowerCase();
						if (gestorFisio.buscarSesion(nombre) == null) {
							System.out.println("no existe la sesi�n");
						} else {
							System.out.println(gestorFisio.buscarSesion(nombre));
						}
						break;
					case 4:
						System.out.println("4.- Listar todas las sesiones");
						gestorFisio.listarSesiones();
						break;
					case 5:
						System.out.println("5.- Listar las sesiones de un trabajador");
						System.out.println("Por favor introduzca el usuario del trabajador");
						usuario = input.nextLine().toLowerCase();
						gestorFisio.listarSesionesPorUsuarioTrabajador(usuario);
						break;
					case 6:
						System.out.println("6.- Asignar una sesi�n a un trabajador");
						System.out.println("Por favor introduzca el usuario del trabajador");
						usuario = input.nextLine().toLowerCase();
						System.out.println("Por favor introduzca el nombre de la sesi�n");
						nombre = input.nextLine().toLowerCase();
						gestorFisio.asignarTrabajadorAUnaSesion(usuario, nombre);
						break;
					case 7:
						System.out.println("Por favor introduzca el nombre de la sesi�n");
						nombre = input.nextLine().toLowerCase();
						System.out.println("Por favor introduzca el atributo que quiere visualizar");
						System.out.println("Puede visualizar: - fechaCompra - nombre - horaInicio");
						System.out.println("- n_materiales - estado - horaFinal - precio - cliente");
						String valor = input.nextLine().toLowerCase();
						gestorFisio.ListarSesionPorAtributo(nombre, valor);
						break;
					case 8:
						System.out.println("8.- Sesi�n mas cara");
						gestorFisio.sesionMasCara();
						break;
					case 9:
						System.out.println("9.- Salir");
						break;
					default:
						System.out.println("Opci�n no valida");
						break;
					}
				} while (opcionTres != 9);
				System.out.println("*****");
				break;

			case 3:
				System.out.println("Has elegido Salir, gracias por usarme!!");
				System.out.println("*****");
				;
				break;

			default:
				System.out.println("Opci�n no valida");
				System.out.println("*****");
				;
				break;
			}
		} while (opcion != 3);
		input.close();

	}

	static void altaTrabajador(GestorSesionesTrabajadores gestorFisio) {
		System.out.println("Por favor introduzca el nombre del trabajador: ");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca el usuario del trabajador");
		String usuario = input.nextLine();
		System.out.println("Por favor introduzca la contrase�a del trabajador");
		String password = input.nextLine();
		System.out.println("Por favor introduzca la fecha de nacimiento del trabajador");
		System.out.println("Formtado de fecha: 1998-09-06");
		String fechaNacimiento = input.nextLine();
		System.out.println("Por favor introduzca la sala del trabajador");
		String sala = input.nextLine();
		System.out.println("Por favor introduzca el salario del trabajador");
		float salario = input.nextFloat();
		gestorFisio.altaTrabajador(nombre, usuario, password, fechaNacimiento, sala, salario);
	}

	static void altaSesion(GestorSesionesTrabajadores gestorFisio) {
		System.out.println("Por favor introduzca el nombre de la sesi�n");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca la hora de inicio de la sesi�n");
		String horaInicio = input.nextLine();
		System.out.println("Por favor introduzca la hora de final de la sesi�n");
		String horaFinal = input.nextLine();
		System.out.println("Por favor introduzca el precio de la sesi�n");
		Float precio = input.nextFloat();
		input.nextLine();
		System.out.println("Por favor introduzca el estado de la sesi�n");
		String estado = input.nextLine();
		System.out.println("Por favor introduzca el nombre del cliente de la sesi�n");
		String cliente = input.nextLine();
		System.out.println("Por favor introduzca el n�mero de materiales que se utilizaran en la sesi�n");
		int n_materiales = input.nextInt();
		gestorFisio.altaSesion(nombre, horaInicio, horaFinal, precio, estado, cliente, n_materiales);
	}
}
